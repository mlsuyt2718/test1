# ĞMixer-py

ĞMixer is a Duniter currency mixer to anonymize transactions. This implementation can 
run in an onion network, guaranteeing a strong anonymity.

## Contribute

There is a lot of things to do. You also can create a client, for exemple in JS.

It would be great to have a logo (maybe with a mixer blade and an onion in a Ğ), and schemas to explain the protocol.

## How to install it

You can install the gmixer with the script install_gmixer.sh (works on Debian derivated system for the moment)

## How to use it

Install dependances:

	sudo cp lib/libleveldb.a /usr/lib/libleveldb.a

`libleveldb-dev=>1.20` is required for `plyvel`. You can copy it from this repository with the command above. You can also build it: If you can only get an older version with your package manager, [build it by hand](https://github.com/google/leveldb/releases). (currently it it the case for Debian Stretch, not for Archlinux)

Install the following packages: `libleveldb-dev` `libsodium-dev` `python3` `python3-pip`

    sudo pip3 install libnacl duniterpy silkaj py-ubjson plyvel PySocks
    
    python3 --version
    # If Python < 3.6:
    sudo pip3 install python2_secrets

Create config & data dir: (change `~/.gmixer-g1`, so you can run different 
servers with different configs) (note that each server must have different dir)

    python3 server.py -i -d ~/.gmixer-g1

Edit config and set your values. If you are not using any proxy, `bind_` and `public_` addresses should be the same.
If `salt` or `password` are empty, they will be asked at runtime. For security reasons, it is not a good idea to write them in commandline.

Now, start the server:

    python3 server.py -s
    # Use -d if using a different dir than default
    # Use -P for automatic fetch of public IP address

### Proxy

To use a SOCKS5 proxy for client connections, set the `client.proxy` value in `config.json` as following:

    "proxy": ["127.0.0.1", 8080], // ["address", port] or null
    "proxy_onion_only": false

To use a proxy only when connecting to a `.onion` address, set the `client.proxy_onion_only` value to `true`:


    "proxy": ["127.0.0.1", 9050],
    "proxy_onion_only": true

If using a reverse proxy (i.e. Tor hidden service), example:

    "bind_host": "127.0.0.1", # local address set in torrc file
    "bind_port": 10951,
    "public_host": "svetsae7j3usrycn.onion", // other nodes contact me with this address
    "public_port": 10951

To unactivate proxy, set `client.proxy` to `null`.

## Protocols

### Peer list file

    pubkey host port
    ...

Example:

    DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr localhost 10951
    EiZ8LNJmtwCDQa8W8PcnB9n8Q7QreMHsB24kS14iV5vV 12.34.56.78 443

### HTTP API

URL is used like commandline arguments. Arguments are separated by slashes. Different commands can be set in the same request.

Command: **/list** list known peers (excluding itself)

    // response: (clear json)
    "peers": [
        { "pubkey": "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr", "localhost", "10951" },
        ...
    ]

Command: **/pubkey** get peer pubkey

    // response: (clear json)
    "pubkey": "ENSD6KikoZAcUVGBWJNKFREG2ryrGaHUD68GCYsiY2pK"

Command: **/version** get peer version

    // response: (clear json)
    "version": "0.1.0"

Command: **/new/_pubkey_** tell the peer about a new peer

    // request: (encrypted then signed json)
    "pubkey": "EiZ8LNJmtwCDQa8W8PcnB9n8Q7QreMHsB24kS14iV5vV", // new peer pubkey
    "host": "12.34.56.78", // new peer host
    "port": "443", // new peer port
    "time": 1551444674 // timestamp

Command: **/mix/_pubkey_/_amount_/_base_** mix a transaction

TODO: update; translate into English

Avant d'envoyer une requête mix, le client crée plusieurs clés publiques à usage unique (une par nœud mixeur) dont il garde les identifiants.

L'URL contient la clé publique du compte émetteur de la transaction, ainsi que la somme transmise. Ces données seront utilisées par le nœud ĞMix pour vérifier l'existance de la transaction.

Le contenu de la requête est chiffré avec la clé publique du nœud receveur puis signé par la clé publique du compte émetteur. Il est composé de la clé publique (ASCII, 44 caractères) du compte auquel le nœud doit renvoyer la transaction. Si cette clé correspond à un nœud ĞMix faisant partie du chemin prévu de la transaction, elle doit être immédiatement suivie (sans séparateur) d'une des clés à usage unique puis des données qui doivent être envoyées à ce nœud (syntaxe décrite par ce même paragraphe, signature exclue&nbsp;: pour garder l'anonymat de l'émetteur d'origine, les données doivent être signées par le nœud qui envoie le paquet).

Command: **/confirm/_pubkey_/_comment_** confirm a transaction

TODO: finish; translate into English

Quand le dernier nœud du chemin a reçu la requête `mix` (c-à-d une fois que tous les nœuds ont reçu la commande), celui-ci envoie une requête `confirm` au nœud qui l'a contacté. La requête va ainsi jusqu'au client d'origine.

L'URL contient la clé publique du nœud qui envoie la requête et le commentaire de la transaction qu'il doit recevoir (pour lui `in_comment`, pour le nœud qu'il contacte `out_comment`), qui sert d'ID de transaction entre les deux nœuds.
